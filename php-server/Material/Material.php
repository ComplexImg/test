<?php

namespace Material;

abstract class Material{

    // Этот флаг и определяет безопасно сырье(true) или же радиоактивно(false).
    protected $flag;

    // Наследники могут расширить этот класс и он сможет иметь более 2 состояний, которые можно
    // будет учитывать в более сложных проверках и вычислениях
    final function getStatus()
    {
        return $this->flag;
    }

}