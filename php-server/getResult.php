<?php

if ($_POST == null) {

    spl_autoload_register();

    $data = require_once "classNames.php";

    $acceptor = new Acceptor($data, $_POST);

    $acceptor->createClient();

    $acceptor->getResult();
}
