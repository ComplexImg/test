<?php
return array(
    "names" => array(
        "Пришелец" => "AlienClient",
        "Плохой клиент" => "BadClient",
        "Хороший клиент" => "GoodClient",
    ),
    "materials" => array(
        "Радиоактивное сырье" => "RadioactiveMaterial",
        "Хорошее сырье" => "GoodMaterial",
    ),
    "other" => array(
        "Да, пьян" => 0,
        "Нет, не пьян" => 1,
    )
);