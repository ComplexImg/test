<?php

class Acceptor
{

    private $data = null;
    private $post = null;
    private $client = null;


    function __construct(array $data, array $post)
    {
        $this->data = $data;
        $this->post = $post;
    }

    // На основе полученных данных с формы и данных массива, создаем и инициализируем нужные классы
    function createClient()
    {

        $clientName = $this->post['name'];
        $materialType = $this->post["material"];
        $otherValue = $this->post['other'];


        $materialClassName = "Material\\" . $this->data['materials'][$materialType];
        $clientClassName = "Client\\" . $this->data['names'][$clientName];
        $otherValue = $this->data['other'][$otherValue];

        if (class_exists($materialClassName)) {
            $material = new $materialClassName();
            if (class_exists($clientClassName)) {
                $client = new $clientClassName($otherValue);

                $client->setMaterial($material);

                $this->client = $client;
            }
        }
    }

    // возвращаем результат в формате json
    function getResult()
    {
        echo json_encode(array("result" => $this->client->getResult()));
    }

}