<?php

namespace Client;

class GoodClient extends Client{

    // Настроение рабочего при приеме сырья от обычного человека зависит лишь от типа сырья, его и возвращаем
    public function getResult(): bool
    {
        return $this->material->getStatus();
    }

}