<?php

namespace Client;

use Material\Material;

abstract class Client{

    // Каждый клиент имеет при себе сырье. Оно хранится в этой переменной
    protected $material;


    final function setMaterial(Material $material)
    {
        $this->material = $material;
    }

    // Данный метод уникален для каждого типа клиента, тк у разных клиентов учитываются разные их параметры.
    // Содержит код, который должен возвращает настроение рабочего (true - хорошее)
    abstract function getResult(): bool;

}