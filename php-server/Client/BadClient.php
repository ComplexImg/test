<?php

namespace Client;

class BadClient extends Client{

    // В отличие от остальных клиентов, ассоц. личность имеет дополнительный параметр в виде опьянения.
    private $drunkenness = null;

    function __construct(bool $drunkenness)
    {
        $this->drunkenness = $drunkenness;
    }

    // Опьянение учитывается в расчете настроения рабочего
    public function getResult(): bool
    {
        return ! $this->drunkenness || $this->material->getStatus();
    }

}