<?php
return array(
    array(
        "name" => "Пришелец",
        "materials" => array(
            "Радиоактивное сырье"
        ),
        "other" => null
    ),
    array(
        "name" => "Плохой клиент",
        "materials" => array(
            "Радиоактивное сырье",
            "Хорошее сырье"
        ),
        "other" => array(
            "Да, пьян",
            "Нет, не пьян"
        )
    ),
    array(
        "name" => "Хороший клиент",
        "materials" => array(
            "Радиоактивное сырье",
            "Хорошее сырье"
        ),
        "other" => null
    )
);