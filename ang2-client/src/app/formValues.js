"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var formValues = (function () {
    function formValues(name, material, other) {
        this.name = name;
        this.material = material;
        this.other = other;
    }
    return formValues;
}());
exports.formValues = formValues;
//# sourceMappingURL=formValues.js.map