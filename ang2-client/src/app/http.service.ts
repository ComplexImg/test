// Imports
import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Client }           from './client';
import { Result }           from './result';
import {formValues} from './formValues';
import {Observable} from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HttpService {

    constructor (private http: Http) {
        this.http = http;
    }

    private headers = new Headers({'Content-Type': 'application/json'});

    getClients() : Promise<Client[]>{
        return this.http.get('http://ppv/app/getData.php')
            .toPromise()
            .then(response => response.json().data as Client[])
            .catch(this.handleError);
    }


    getResult(formValues: formValues) : Promise<Result> {
        const url = `http://ppv/app/getResult.php`;
        return this.http
            .post(url, JSON.stringify(formValues), {headers: this.headers})
            .toPromise()
            .then(response => response.json().data as Result)
            .catch(this.handleError);
    }


    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
