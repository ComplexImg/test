import {Component, OnInit} from '@angular/core';
import {HttpService} from './http.service';

import { Result }           from './result';
import {Client} from './client';
import {formValues} from './formValues';


@Component({
    selector: 'my-app',
    templateUrl: "app.component.html",
    providers: [HttpService]
})
export class AppComponent implements OnInit {

    Clients: Client[];
    Result: Result;
    formValues: formValues;

    result: string;

    constructor(private httpService: HttpService) {
        this.httpService = httpService;
    }


    getResult(): void {

        console.log(this.formValues);
        console.log("---------------");
        
         // Обращение к серверу и передача ему данных методом пост, получение результата
        this.httpService
            .getResult(this.formValues)
            .then(res => this.Result = res);

    }

    ngOnInit() {
   
    // Инициализация списка клиентов для дальнейшей отрисовки формы (данные с сервера методом гет)
        this.httpService
            .getClients()
            .then(cl => this.Clients = cl);



        this.Result = {result:null};

        this.formValues = {name: '', material: '', other: ''};

        // Статическая инициализация
        // this.Clients = [
        //     {name: "Пришелец", materials: ["Радиоактивное сырье"], other: null},
        //     {
        //         name: "Ассоц. личность",
        //         materials: ["Радиоактивное сырье", "Хорошее сырье"],
        //         other: ["Да, пьян", "Нет, не пьян"]
        //     },
        //     {name: "Хороший клиент", materials: ["Радиоактивное сырье", "Хорошее сырье"], other: null},
        // ];

    }
}