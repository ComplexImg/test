"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Client = (function () {
    function Client(name, materials, other) {
        this.name = name;
        this.materials = materials;
        this.other = other;
    }
    return Client;
}());
exports.Client = Client;
//# sourceMappingURL=client.js.map