"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_service_1 = require("./http.service");
var AppComponent = (function () {
    function AppComponent(httpService) {
        this.httpService = httpService;
        this.httpService = httpService;
    }
    AppComponent.prototype.getResult = function () {
        console.log(this.formValues);
        console.log("---------------");
        
        // Обращение к серверу и передача ему данных методом пост, получение результата
         this.httpService
             .getResult(this.formValues)
             .then(res => this.Result = res);
    };
    AppComponent.prototype.ngOnInit = function () {
        
        // Инициализация списка клиентов для дальнейшей отрисовки формы (данные с сервера методом гет)
         this.httpService
             .getClients()
             .then(cl => this.Clients = cl);
        
        
        this.Result = { result: null };
        this.formValues = { name: '', material: '', other: '' };
        
        // Статическая инициализация
        // this.Clients = [
        //     { name: "Пришелец", materials: ["Радиоактивное сырье"], other: null },
        //     {
        //         name: "Ассоц. личность",
        //         materials: ["Радиоактивное сырье", "Хорошее сырье"],
        //         other: ["Да, пьян", "Нет, не пьян"]
        //     },
        //     { name: "Хороший клиент", materials: ["Радиоактивное сырье", "Хорошее сырье"], other: null },
        // ];
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'my-app',
        templateUrl: "app.component.html",
        providers: [http_service_1.HttpService]
    }),
    __metadata("design:paramtypes", [http_service_1.HttpService])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map